<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cost extends Model
{
    use HasFactory;

    protected $table = "costs";

    public function children($projectId){
    	return CostType::join('costs','costs.cost_type_id','cost_types.id')
    					->where('costs.project_id',$projectId)
    					->select('cost_types.*')
    					->get();
    }
}
