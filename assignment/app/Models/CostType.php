<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CostType extends Model
{
    use HasFactory;

    protected $table = "cost_types";

    public function children() {
        return $this->hasMany(CostType::class, 'parent_id', 'id'); 
    } 
	
    public function parent() {
        return $this->belongsTo(CostType::class, 'parent_id','id'); 
    }

}
