<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $table = "clients";

    protected $appends = [
		'amount',
		'type',
    ];

    public function project()
    {
		if(request()->project_id){
			return $this->hasMany(Project::class)
						->whereIn('id',request()->project_id);
		}
		else{
			return $this->hasMany(Project::class);
		}
    }

    public function getAmountAttribute(){
    	return $this->project->sum('amount');
    }

    public function getTypeAttribute(){
    	return "client";
    }

}
