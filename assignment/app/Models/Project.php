<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;
    protected $table = "projects";    

    protected $appends =[
    	'amount',
    	'type',
    	'children',
    ];

    public function cost()
    {
        return $this->hasMany(Cost::class);
    }

	public function getAmountAttribute(){
    	return $this->getChildCostTypes(NULL)->sum('amount');
    }

    public function getTypeAttribute(){
    	return "project";
    }

    public function getChildrenAttribute(){
        $costTypeIds = $this->getChildCostTypes(NULL,$costTypes='');
        return $costTypeIds;
    }

    public function getChildCostTypes($costTypeParentId=NULL,$costTypes=''){
        if (!is_array($costTypes))
            $costTypes = array();

        $costTypes =  CostType::where('parent_id',$costTypeParentId)
                    ->join('costs','costs.cost_type_id','=','cost_types.id')
                    ->where('costs.project_id',$this->id);
        
        if(request()->cost_type_id){
            $ids = request()->cost_type_id;
            foreach(request()->cost_type_id as $key => $value){
                $id = $value;
                while($id){                
                    $id = CostType::where('id',$id)->pluck('parent_id')->toArray();
                    if(count($id) && $id[0]){
                        $ids[] = $id[0];
                        $id = $id[0];
                        continue;
                    }
                    else{
                        break;
                    }
                }
            }
            $costTypes = $costTypes->where(function($q) use($ids){
                foreach($ids as $id){
                    $q->orWhere('cost_types.id',$id);
                }
            });
        }
        $costTypes = $costTypes->select('cost_types.*','costs.amount')
                    ->get();

        foreach($costTypes as $k=>$v){
            $children = $this->getChildCostTypes($v->id,$costTypes);
            if(!empty($children)){
                $v->children = $children;
            }
            
        }
                    
        return $costTypes;
    }
}
