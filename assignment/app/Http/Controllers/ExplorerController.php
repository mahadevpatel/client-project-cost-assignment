<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Client;
use App\Models\Project;


class ExplorerController extends Controller
{
    public function __construct(){}


    public function index ( Request $request ){

	    $clients = (new Client)->newQuery();
		
		if($request->client_id){
			$clients = $clients->whereIn('id',$request->client_id);
		}
		if($request->project_id){
			$clients = $clients->join('projects','clients.id','=','projects.client_id')
							->whereIn('projects.id',$request->project_id)
							->distinct('clients.id')
							->select('clients.*');
		}
		$clients = $clients->with('project')->get();

		return $clients; 
	}
}
